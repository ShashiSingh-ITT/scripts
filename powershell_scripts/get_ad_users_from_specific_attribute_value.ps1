foreach($arg in $args){
    $filename = $args[0]

}


$valid_date = ((Get-Date).AddDays(0)).Date
try{
    $emails = Get-ADUser -Filter {whenCreated -ge $valid_date -and EmployeeID -like "*"}  -Properties * | Select-Object mail
}catch{
    $exception_message =  $_
}finally{
    if($emails -ne $null){
        $emails.mail| out-file -FilePath $filename
    }
}

if(Test-Path -Path $filename){
    return $true
}else{
    return $exception_message
}
