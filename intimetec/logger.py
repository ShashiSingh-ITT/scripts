import datetime,os
from pathlib import Path

# BASE_DIR = Path(__file__).resolve().parent.parent
# print(BASE_DIR)
#
# def logToFile(content,log_folder_name):
#     currentDate = datetime.datetime.now().date()
#     if not os.path.exists("""{}\\{}\\logs""".format(BASE_DIR,log_folder_name)):
#         os.makedirs("""{}\\{}\\logs""".format(BASE_DIR,log_folder_name))
#     fileName = "{}\\{}\\logs\\{}.txt".format(BASE_DIR,log_folder_name,currentDate)
#     file = open(fileName,"a")
#     file.write("{}: {} \n".format(datetime.datetime.now(),content))
#     file.close()



def log(content,path,filename):
    if not os.path.exists(path):
        os.makedirs(path)
    # filename = str(datetime.datetime.now().date())+".txt"
    filepath = """{}\\{}""".format(path,filename)
    file = open(filepath,"a")
    file.write("{}: {} \n".format(datetime.datetime.now(),content))
    file.close()


# print (log(content="this is the test content",path="C:\\scripts\\dl_mover\\execution_logs"))