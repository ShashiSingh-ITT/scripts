

def read_content_from_file_line_by_line(filename):
    content_in_list = []
    with open(filename,encoding="UTF-16") as file:
        for line in file:
            content_in_list.append(line.rstrip())
    return content_in_list
