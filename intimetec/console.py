import json
import subprocess


def execute(command=None, use_cmd=False, decoder=None):
    print ("command",command)
    status, result = [False, '']
    if isinstance(command, str) and command.strip() != "":
        try:
            output = subprocess.run(f"""PowerShell.exe "{command}""", capture_output=True, shell=True)
            print ("out",output)
            if bool(output.returncode == 0):
                status, result = [True, str(output.stdout.decode())]
            else:
                status, result = [False, str(output.stderr.decode())]

        except Exception as e:
            status, result = [False, f"exception {e} occurred while executing the command : {command}"]

    print(status,result)
    return status, result




