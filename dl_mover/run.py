from intimetec.console import execute
from intimetec import logger
import os,datetime
from pathlib import Path

from intimetec.file_reader import read_content_from_file_line_by_line

def add_member_to_ADGROUP(groupname,membername):
    command = """Add-ADGroupMember -Identity '{}' -Members {}""".format(groupname,membername)
    status,result = execute(command=command)
    if status == True and str(result).strip() == "":
        return True
    else:
        return False




BASE_DIR = Path(__file__).resolve().parent
data_log_path = BASE_DIR.joinpath("data_logs") #C:\scripts\dl_mover\data_logs
now = datetime.datetime.now()
current_time_string = now.strftime("%m%d%Y-%H%M%S")
userlist_out_file_path = """{}""".format(data_log_path)
execution_log_file_path = str(BASE_DIR.joinpath("execution_logs"))
log_file_name = """{}.txt""".format(current_time_string)
data_log_file_name = """{}.txt""".format(current_time_string)
execution_log_file_name = """{}.txt""".format(str(datetime.datetime.now().date()))
print("userlist_out_file_path",userlist_out_file_path)
print("execution_log_file_path",execution_log_file_path)

if not os.path.exists(userlist_out_file_path):
    os.makedirs(userlist_out_file_path)
status,result = execute(command="""{}\powershell_scripts\get_ad_users_from_specific_attribute_value.ps1 {}""".format(
    BASE_DIR.parent,"{}\\{}".format(userlist_out_file_path,data_log_file_name))
)

print ("status:",status,"result:",result)

if status==True and str(result).strip()=="":
    logger.log(content="No new users found for the day",path=execution_log_file_path,filename=execution_log_file_name)
elif status==True and str(result).strip().lower()=="true":
    logger.log(content="New user list generated at {}\\{}".format(userlist_out_file_path,data_log_file_name),path=execution_log_file_path,filename=execution_log_file_name)
    try:
        newly_created_users_list = read_content_from_file_line_by_line(filename="{}\\{}".format(userlist_out_file_path,data_log_file_name))
        print ("newly_created_users_list",newly_created_users_list)
        status_of_member_added_to_group = {}
        if len(newly_created_users_list)==0:
            logger.log(content="0 users created today",path=execution_log_file_path,filename=execution_log_file_name)
        else:
            group_names = ["All-Employees-AD","O365 E1 License"]
            for group_name in group_names:
                status_of_member_added_to_group[group_name]={'success':[],'failure':[]}
                for user in newly_created_users_list:
                    user = str(user.split("@")[0]).strip()
                    if add_member_to_ADGROUP(groupname=group_name,membername=user)==True:
                        status_of_member_added_to_group[group_name]['success'].append(user)
                    else:
                        status_of_member_added_to_group[group_name]['failure'].append(user)
            logger.log(content=status_of_member_added_to_group,path=execution_log_file_path,filename=execution_log_file_name)
    except Exception as e:
        logger.log(content="Exception Occured - {}".format(e),path=execution_log_file_path,filename=execution_log_file_name)
else:
    logger.log(content=result,path=execution_log_file_path,filename=execution_log_file_name)
